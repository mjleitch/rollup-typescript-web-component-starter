import cjs from 'rollup-plugin-commonjs'
import globals from 'rollup-plugin-node-globals'
import replace from 'rollup-plugin-replace'
import resolve from 'rollup-plugin-node-resolve'
import typescript from 'rollup-plugin-typescript'

export default {
  input: 'src/components/my-button/my-button.ts',
  name: 'MyButton',
  plugins: [
    typescript({
      typescript: require('typescript')
    }),
    cjs({
      exclude: 'node_modules/process-es6/**',
      include: []
    }),
    globals(),
    replace({ 'process.env.NODE_ENV': JSON.stringify('development') }),
    resolve({
      browser: true,
      main: true
    })
  ],
  output: {
    sourcemap: true,
    format: 'iife',
    file: 'build/my-button.js'
  }
}
